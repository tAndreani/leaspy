leaspy.models.multivariate\_model module
========================================

.. automodule:: leaspy.models.multivariate_model
   :members:
   :undoc-members:
   :show-inheritance:
