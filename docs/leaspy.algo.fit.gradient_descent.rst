leaspy.algo.fit.gradient\_descent module
========================================

.. automodule:: leaspy.algo.fit.gradient_descent
   :members:
   :undoc-members:
   :show-inheritance:
