leaspy.algo.personalize.gradient\_descent\_personalize module
=============================================================

.. automodule:: leaspy.algo.personalize.gradient_descent_personalize
   :members:
   :undoc-members:
   :show-inheritance:
