leaspy.models.abstract\_multivariate\_model module
==================================================

.. automodule:: leaspy.models.abstract_multivariate_model
   :members:
   :undoc-members:
   :show-inheritance:
