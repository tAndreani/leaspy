leaspy.algo.samplers.gibbs\_sampler module
==========================================

.. automodule:: leaspy.algo.samplers.gibbs_sampler
   :members:
   :undoc-members:
   :show-inheritance:
