leaspy.algo.personalize.scipy\_minimize module
==============================================

.. automodule:: leaspy.algo.personalize.scipy_minimize
   :members:
   :undoc-members:
   :show-inheritance:
