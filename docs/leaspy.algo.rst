leaspy.algo package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   leaspy.algo.fit
   leaspy.algo.personalize
   leaspy.algo.samplers
   leaspy.algo.simulate
   leaspy.algo.others

Submodules
----------

.. toctree::
   :maxdepth: 4

   leaspy.algo.abstract_algo
   leaspy.algo.algo_factory

Module contents
---------------

.. automodule:: leaspy.algo
   :members:
   :undoc-members:
   :show-inheritance:
