leaspy.models.multivariate\_parallel\_model module
==================================================

.. automodule:: leaspy.models.multivariate_parallel_model
   :members:
   :undoc-members:
   :show-inheritance:
